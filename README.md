# · FOR EXPERIMENTAL USE ONLY ·
This stuff is "still under development" and highly dangerouse - use it AT YOUR OWN RISK!

# ArchiveBackPacker

The "ArchiveBackPacker" is a script, which has a arbitrary archive piggyback.<br>
The ArchiveBackPacker-script can be executed via a webserver/browser and it
extracts its own "piggyback-archive" to any target location itself (the
target-location is hard-defined inside the script at this moment).

# Usage

It is assumed, you have a webspace, which holds your (source-)project and at which
you have "full"-access.

* E.g. you create an archive of your project-stuff in that folder.
* You place the `builder.php` inside that project-folder, near by the new created archive.
* With your browser you call the biulder-script. (`http://..../builder.php`)
* After that you're end up with a `named`-script (the name is hard-defined
  inside the script at the moment), which your archive has piggyback and
  can be placed anywhere at a other webspace.<br>
  There, now you can call it via a browser to unload the "backpack-archive"
  and do what you want with it (or let the ArchiveBackPacker-script do something
  whith that archive).

To define the actions, the ArchiveBackPacker can do, after you have
transferred it to your target location, you have to modify the ArchiveBackPacker-code
inside the `builder.php`-script, to generated a appropriate ArchiveBackPacker.

