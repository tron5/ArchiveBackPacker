<?php
/**
 * Generator of the "ArchiveBackPacker"-file
 *
 * The "ArchiveBackPacker" is a script, which has a arbitrary archive piggyback.
 * The "ArchiveBackPacker"-file can be executed via a webserver/browser and it
 * extracts his "piggyback-archive", the "payload", to any target location by itself.
 *
 * - callable via browser
 * - needs the archive, which is to transport, in its current folder
 */

$szPayloadName = 'myphar.tar.gz'; // the project-archive (source/sources)
$szABPName     = 'abp.php';       // the resulting ArchiveBackPacker-file, ready for transport/execution


// create the "ArchiveBackPacker"-script as a string
$szABPScript = <<<FILECONTENT
<?php
echo 'ABP-script is running..<br>';
echo __FILE__.'<br>';
\$nScriptLen = (int)(str_replace('0', '', '#SIZEPLACEHOLDER#'));
echo 'script-size is: '.\$nScriptLen.'<br>';
\$fhs = fopen(__FILE__, 'r');
fseek(\$fhs, 0, SEEK_END);
echo 'file-size is: '.ftell(\$fhs);

fseek(\$fhs, \$nScriptLen);
\$fho = fopen('output.tar.gz', 'w+');
while (!feof(\$fhs)) {
    \$chunk = fread(\$fhs, 1024);
    fwrite(\$fho, \$chunk);
}
fclose(\$fho);
fclose(\$fhs);
__HALT_COMPILER();
FILECONTENT;

// determine the length of the "ArchiveBackPacker"-script itself
$szABPScriptLen = strlen($szABPScript);
$nFillUp        = (strlen('#SIZEPLACEHOLDER#') - strlen($szABPScriptLen));
for ($i = 0; $i < $nFillUp; $i++) {
    $szABPScriptLen = '0'.$szABPScriptLen;
}
// parses the script-size in the script itself (and do not alter the script-size!)
$szABPScript = str_replace('#SIZEPLACEHOLDER#',(string) $szABPScriptLen, $szABPScript);

// write the "ArchiveBackPacker"-script into a regular file
$fh = fopen($szABPName, 'w+');
fwrite($fh, $szABPScript);
fflush($fh);
fclose($fh);

// append the payload (the archive) at the end of the updater-script
$fhr = fopen($szPayloadName, 'rb');
$fht = fopen($szABPName, 'a');
while (!feof($fhr)) {
    $chunk = fread($fhr, 1024);
    fwrite($fht, $chunk);
}
$nABPFileSize = fstat($fht)['size'];
fclose($fht);
fclose($fhr);


// further actions of this "builder"-script:
//
// OPTIONAL execute "ArchiveBackPacker" imediately:
// redirect to the ArchiveBackPacker-script to execute it
//
/*
 *$szRedirectTarget = $_SERVER['SERVER_NAME'].str_replace(basename(__FILE__), '', $_SERVER['REQUEST_URI']).$szABPName;
 *header("Location: http://".$szRedirectTarget);
 */

// OPTIONAL download the "ArchiveBackPacker"-file:
// this way, the ArchiveBackPacker-file can be transported and later executed anywhere
//
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="' . $szABPName . '"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . $nABPFileSize);
readfile($szABPName);
unlink($szABPName);  // if we provide a download, we did not need the archive-backpack anymore here

